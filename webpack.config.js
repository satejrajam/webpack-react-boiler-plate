var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

const path = require("path");

module.exports = {

	entry: "./src/app.js",
	output: {	
		path: path.resolve(__dirname, "dist"),	
		filename: "app.bundle.js"
	},
	module:{
		rules:[
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
					fallback: "style-loader",
					use: ['css-loader','sass-loader'],
					publicPath: 'dist/'
				})

			},
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: "babel-loader"
			}
		]
	},
	devServer:{
	  contentBase: path.join(__dirname, "dist"),
	  compress: true,
	  stats: "errors-only",
	  open: true
	},
  	plugins: [
  		new HtmlWebpackPlugin({
		    title: 'Webpack|React App',
		    template: './src/index.html',
  		}),
	 	new ExtractTextPlugin({
	 				filename: 'app.css',
	 				disable: false,	 				
	 				allChunks: true
	 			})
	]

}