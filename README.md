# README #

This boiler plate consists of webpack-2 configuration with react, babel, sass loaders.

- Clone it
- Run the following commands

```
#!Terminal

$ npm i
$ npm run dev
```